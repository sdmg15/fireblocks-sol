import * as sol from "@solana/web3.js"

async function connect(host: string) { 
    const con = new sol.Connection(host, 'recent');
    return con;
}

async function submitTransaction(signedTx: Buffer) {
    let rpc = await connect(process.env.SOLANA_RPC_URL!);
    await sol.sendAndConfirmRawTransaction(rpc, signedTx)
}

async function createWallet() {
    let account = sol.Keypair.generate();
    return account;
}


export default {submitTransaction, createWallet};