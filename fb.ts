import * as fb from "fireblocks-sdk"


async function signTx(transaction) {
    const fbClient = new fb.FireblocksSDK(process.env.FIREBLOCKS_API_KEY!, process.env.FIREBLOCKS_API_SECRET!);
    const fbSig = await fbClient.getVaultAccountById("john@doe.com");
    transaction.addSignature(fbSig);
    return transaction;
}

async function mint(assetId: any, amount: any, destId: any) { 
    const fbClient = new fb.FireblocksSDK(process.env.FIREBLOCKS_API_KEY!, process.env.FIREBLOCKS_API_SECRET!);

    const payload = {
        assetId,
        amount,
        operation: fb.TransactionOperation.MINT,
        destination: {
            type: fb.PeerType.VAULT_ACCOUNT,
            id: String(destId)
        },
        note: "Minting sols"
    };
    
    const result = await fbClient.createTransaction (payload);
    console.log(JSON.stringify(result, null, 2));
}

async function makeNewVault(name: string) {
    const fbClient = new fb.FireblocksSDK(process.env.FIREBLOCKS_API_KEY!, process.env.FIREBLOCKS_API_SECRET!);
    const acc = await fbClient.createVaultAccount(name);

    const vault = { 
        vaultName: acc.name,
        vaultID: acc.id 
    };

    const vaultWallet = await fbClient.createVaultAsset(vault.vaultID, "SOL");
    return vaultWallet;
}

export default fb;